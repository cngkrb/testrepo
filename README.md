run -> mvn clean package (mapstruct needs to generate classes) <br />
http://localhost:4540/swagger-ui.html#/ <br />

tools:  <br />
sts 3.9.4  <br />
java 1.8  <br />
eclemma (for test coverage)  <br />

changelog
- functional bugs fixed
- swagger documentation expanded
- Currency validator and CardNumber validator added
- Controller test coverage is now %100

