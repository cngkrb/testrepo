package com.cs.mywallet.api.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.cs.mywallet.api.controller.model.ClientModel;
import com.cs.mywallet.api.dao.ClientRepository;
import com.cs.mywallet.api.dao.model.Client;
import com.cs.mywallet.api.exception.ClientNotFoundException;

public class ClientServiceTests {
	private ClientRepository clientRepository;

	private ClientService clientService;

	@Before
	public void setup() {
		this.clientRepository = mock(ClientRepository.class);
		this.clientService = new ClientServiceImpl(clientRepository);
	}

	@Test
	public void givenClient_whenGetClientById_thenReturnClient() throws Exception {
		Long clientId = 1L;
		Client client = new Client(clientId, "Cengiz", true);
		when(clientRepository.findById(clientId)).thenReturn(Optional.of(client));
		ClientModel response = clientService.getClientById(clientId);
		assertEquals(response.getName(), client.getName());
		// TODO diğer alanlarıda ekle
	}

	@Test(expected = ClientNotFoundException.class)
	public void givenNullClient_whenGetClientById_thenShouldReturnNotFound() throws Exception {
		Long clientId = 1L;
		when(clientRepository.findById(clientId)).thenReturn(Optional.empty());
		clientService.getClientById(clientId);
	}
}
