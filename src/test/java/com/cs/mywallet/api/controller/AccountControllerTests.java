package com.cs.mywallet.api.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.cs.mywallet.api.controller.model.AccountModel;
import com.cs.mywallet.api.controller.model.CreateAccountModel;
import com.cs.mywallet.api.controller.model.CreditCardDepositModel;
import com.cs.mywallet.api.controller.model.TransactionDetailModel;
import com.cs.mywallet.api.dao.AccountRepository;
import com.cs.mywallet.api.dao.ClientRepository;
import com.cs.mywallet.api.dao.TransactionRepository;
import com.cs.mywallet.api.dao.model.Account;
import com.cs.mywallet.api.dao.model.Client;
import com.cs.mywallet.api.dao.model.Transaction;
import com.cs.mywallet.api.exception.CustomizedResponseEntityExceptionHandler;
import com.cs.mywallet.api.service.CreditCardProvisionService;
import com.cs.mywallet.api.service.model.CreditCardProvisionRequest;
import com.cs.mywallet.api.service.model.CreditCardProvisionResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccountControllerTests {

    private MockMvc                    mockMvc;

    private ClientRepository           clientRepository;
    private AccountRepository          accountRepository;
    private TransactionRepository      transactionRepository;
    private CreditCardProvisionService creditCardProvisionService;
    private ObjectMapper               om;

    private String                     URL = "/accounts";

    @Before
    public void setup() {
        this.clientRepository = mock(ClientRepository.class);
        this.accountRepository = mock(AccountRepository.class);
        this.transactionRepository = mock(TransactionRepository.class);
        this.creditCardProvisionService = mock(CreditCardProvisionService.class);

        this.om = new ObjectMapper();
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(new AccountController(clientRepository, accountRepository, transactionRepository, creditCardProvisionService))
                .setControllerAdvice(new CustomizedResponseEntityExceptionHandler()).build();
    }

    @Test
    public void givenAccount_whenGetAccount_thenReturnAccount() throws Exception {
        Long accountId = 1L;
        Account account = new Account(accountId, 1L, new BigDecimal(10), "Cengiz Hesap", "GBP", true);
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        MvcResult result = mockMvc.perform(get(URL + "/{id}", accountId))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
                .andReturn();
        AccountModel response = om.readValue(result.getResponse().getContentAsString(),
                new TypeReference<AccountModel>() {
                });

        assertEquals(response.getName(), account.getName());
        // TODO diğer alanlarıda ekle
    }

    @Test
    public void givenAccount_whenGetAccount_withNotValidAccount_thenReturnNotFound() throws Exception {
        Long accountId = 1L;
        when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        mockMvc.perform(get(URL + "/{id}", accountId))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void whenGetTransactions_withNotValidAccount_thenReturnNotFoundAccount() throws Exception {
        Long accountId = 1L;
        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());
        mockMvc.perform(get(URL + "/{id}/transactions", accountId))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenClient_whenGetAccount_thenReturnAccount() throws Exception {
        Long clientId = 11L;
        Account account = new Account(1l, clientId, new BigDecimal(10), "Cengiz Hesap Test", "TRY", true);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        when(accountRepository.findByClientIdAndActive(clientId, true)).thenReturn(accounts);

        MvcResult result = mockMvc.perform(get(URL + "/client/{clientId}", clientId))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
                .andReturn();

        List<AccountModel> response = om.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<AccountModel>>() {
                });

        AccountModel accountModel = response.get(0);

        assertEquals(accountModel.getName(), account.getName());
        // TODO diğer alanlarıda ekle
    }

    @Test
    public void givenClientAndCurrency_whenGetAccount_thenReturnAccount() throws Exception {
        Long clientId = 11L;
        String currency = "TRY";
        Account account = new Account(1l, clientId, new BigDecimal(10), "Cengiz Hesap Test", "TRY", true);

        when(accountRepository.findByClientIdAndCurrencyAndActive(clientId, currency, true))
                .thenReturn(Optional.of(account));

        MvcResult result = mockMvc.perform(get(URL + "/client/{clientId}", clientId).param("currency", "TRY"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
                .andReturn();

        List<AccountModel> response = om.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<AccountModel>>() {
                });
        AccountModel accountModel = response.get(0);

        assertEquals(accountModel.getName(), account.getName());
        // TODO diğer alanlarıda ekle
    }

    @Test
    public void givenClientAndCurrency_whenGetAccount_withInvalidCurrency_thenReturnBadRequest() throws Exception {
        mockMvc.perform(get(URL + "/client/{clientId}", 11l).param("currency", "TRY1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void givenClientAndCurrency_whenGetAccount_withNotValid_thenReturnNotFound() throws Exception {
        Long clientId = 11L;
        String currency = "TRY";

        when(accountRepository.findByClientIdAndCurrencyAndActive(clientId, currency, true))
                .thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/client/{clientId}", clientId).param("currency", currency))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void givenClient_whenGetAccount_withAccountNotExists_thenReturnNotFound() throws Exception {
        Long clientId = 11L;

        when(accountRepository.findByClientIdAndActive(clientId, true)).thenReturn(Collections.emptyList());

        mockMvc.perform(get(URL + "/client/{clientId}", clientId))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isNotFound())
                .andReturn();

    }

    @Test
    public void givenAccount_whenDeleteAccount_thenReturnNoContent() throws Exception {
        Long accountId = 1L;
        Account account = new Account(accountId, 1L, new BigDecimal(10), "Cengiz Hesap", "GBP", true);

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));

        mockMvc.perform(delete(URL + "/{id}", accountId)).andExpect(status().isNoContent()).andReturn();

    }

    @Test
    public void whenDeleteAccount_withNotValidACcount_thenReturnNoContent() throws Exception {
        Long accountId = 1L;
        when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/{id}", accountId)).andExpect(status().isNotFound()).andReturn();

    }

    @Test
    public void givenTransactions_whenGetTransactions_thenReturnTransactions() throws Exception {
        Long accountId = 1L;
        Account account = new Account(accountId, 1L, new BigDecimal(10), "Cengiz Hesap", "GBP", true);
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));

        List<Transaction> senders = new ArrayList<>();
        senders.add(new Transaction());
        when(transactionRepository.findBySender(any(String.class))).thenReturn(senders);

        List<Transaction> receivers = new ArrayList<>();
        receivers.add(new Transaction());
        when(transactionRepository.findByReceiver(any(String.class))).thenReturn(receivers);

        MvcResult result = mockMvc.perform(get(URL + "/{id}/transactions", accountId))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
                .andReturn();
        TransactionDetailModel response = om.readValue(result.getResponse().getContentAsString(),
                new TypeReference<TransactionDetailModel>() {
                });

        assertEquals(response.getSend().size(), senders.size());
        assertEquals(response.getReceived().size(), receivers.size());
        // TODO diğer alanlarıda ekle
    }

    @Test
    public void whenCreateAccount_thenShouldCreatedAccount() throws Exception {
        CreateAccountModel model = new CreateAccountModel();
        model.setBalance(new BigDecimal(50));
        model.setClientId(1L);
        model.setCurrency("GBP");
        model.setName("Cengiz Hesap");
        Client client = new Client(model.getClientId(), "Cengiz", true);
        when(clientRepository.findById(model.getClientId())).thenReturn(Optional.of(client));
        when(accountRepository.findByClientIdAndCurrencyAndActive(model.getClientId(), model.getCurrency(), true))
                .thenReturn(Optional.empty());

        when(accountRepository.save(any(Account.class))).thenAnswer(new Answer<Account>() {

            @Override
            public Account answer(InvocationOnMock invocation) throws Throwable {
                return new Account(1L, model.getClientId(), model.getBalance(), model.getName(), model.getCurrency(),
                        true);
            }

        });

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(model)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenCreateAccount_withNullBalance_thenShouldCreatedAccount() throws Exception {
        CreateAccountModel model = new CreateAccountModel();
        model.setClientId(1L);
        model.setCurrency("GBP");
        model.setName("Cengiz Hesap");
        Client client = new Client(model.getClientId(), "Cengiz", true);
        when(clientRepository.findById(model.getClientId())).thenReturn(Optional.of(client));
        when(accountRepository.findByClientIdAndCurrencyAndActive(model.getClientId(), model.getCurrency(), true))
                .thenReturn(Optional.empty());

        when(accountRepository.save(any(Account.class))).thenAnswer(new Answer<Account>() {

            @Override
            public Account answer(InvocationOnMock invocation) throws Throwable {
                return new Account(1L, model.getClientId(), model.getBalance(), model.getName(), model.getCurrency(),
                        true);
            }

        });

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(model)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenCreateAccount_withNotValidClient_thenShouldReturnNotFound() throws Exception {
        CreateAccountModel model = new CreateAccountModel();
        model.setBalance(new BigDecimal(50));
        model.setClientId(1L);
        model.setCurrency("GBP");
        model.setName("Cengiz Hesap");
        when(clientRepository.findById(model.getClientId())).thenReturn(Optional.empty());

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(model)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenCreateAccount_withExistClientIdAndCurrecy_thenShouldReturnConflict() throws Exception {
        CreateAccountModel model = new CreateAccountModel();
        model.setBalance(new BigDecimal(50));
        model.setClientId(1L);
        model.setCurrency("GBP");
        model.setName("Cengiz Hesap");
        Client client = new Client(model.getClientId(), "Cengiz", true);
        when(clientRepository.findById(model.getClientId())).thenReturn(Optional.of(client));
        when(accountRepository.findByClientIdAndCurrencyAndActive(model.getClientId(), model.getCurrency(), true))
                .thenReturn(Optional.of(new Account()));

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(model)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isConflict());
    }

    @Test
    public void whenDepositViaCreditCard_thenReturnTransactions() throws Exception {
        CreditCardDepositModel creditCardDepositModel = new CreditCardDepositModel();
        creditCardDepositModel.setCardNumber("4949000012124747");
        creditCardDepositModel.setAmount(new BigDecimal("100.01"));
        creditCardDepositModel.setAccountId(2l);

        Account account = new Account(creditCardDepositModel.getAccountId(), 1l, new BigDecimal(10), "Cengiz Hesap Test", "TRY", true);

        when(accountRepository.findById(creditCardDepositModel.getAccountId())).thenReturn(Optional.of(account));

        CreditCardProvisionResponse response = new CreditCardProvisionResponse(true, "7bcb441e-d7bb-47f9-ab53-55deb7496ab0");
        when(creditCardProvisionService.getProvision(any(CreditCardProvisionRequest.class))).thenReturn(response);

        when(transactionRepository.save(any(Transaction.class))).thenAnswer(new Answer<Transaction>() {

            @Override
            public Transaction answer(InvocationOnMock invocation) throws Throwable {
                Transaction transaction = new Transaction();
                transaction.setId(1l);
                transaction.setAmount(creditCardDepositModel.getAmount());
                transaction.setReceiver(String.valueOf(creditCardDepositModel.getAccountId()));
                transaction.setSender(creditCardDepositModel.getCardNumber());
                transaction.setTransactionType("CC-Deposit");
                transaction.setProvisionId(response.getProvisionId());
                return transaction;
            }

        });

        mockMvc.perform(post(URL + "/deposit/creditcard").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(creditCardDepositModel)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());

    }

    @Test
    public void whenDepositViaCreditCard_withNotValidAccount_thenReturnNotFound() throws Exception {
        CreditCardDepositModel creditCardDepositModel = new CreditCardDepositModel();
        creditCardDepositModel.setCardNumber("4949000012124747");
        creditCardDepositModel.setAmount(new BigDecimal("100.01"));
        creditCardDepositModel.setAccountId(2l);

        when(accountRepository.findById(creditCardDepositModel.getAccountId())).thenReturn(Optional.empty());

        mockMvc.perform(post(URL + "/deposit/creditcard").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(creditCardDepositModel)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenDepositViaCreditCard_withNoProvision_thenReturnNotAcceptable() throws Exception {
        CreditCardDepositModel creditCardDepositModel = new CreditCardDepositModel();
        creditCardDepositModel.setCardNumber("4949000012124747");
        creditCardDepositModel.setAmount(new BigDecimal("100.01"));
        creditCardDepositModel.setAccountId(2l);

        Account account = new Account(creditCardDepositModel.getAccountId(), 1l, new BigDecimal(10), "Cengiz Hesap Test", "TRY", true);

        when(accountRepository.findById(creditCardDepositModel.getAccountId())).thenReturn(Optional.of(account));

        CreditCardProvisionResponse response = new CreditCardProvisionResponse(false, null);
        when(creditCardProvisionService.getProvision(any(CreditCardProvisionRequest.class))).thenReturn(response);

        mockMvc.perform(post(URL + "/deposit/creditcard").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(om.writeValueAsString(creditCardDepositModel)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotAcceptable());
    }
}
