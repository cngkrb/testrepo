package com.cs.mywallet.api.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.cs.mywallet.api.controller.model.ClientModel;
import com.cs.mywallet.api.controller.model.CreateClientModel;
import com.cs.mywallet.api.dao.ClientRepository;
import com.cs.mywallet.api.dao.model.Client;
import com.cs.mywallet.api.exception.CustomizedResponseEntityExceptionHandler;
import com.cs.mywallet.api.service.ClientService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClientControllerTests {

	private MockMvc mockMvc;

	private ClientRepository clientRepository;

	private ClientService clientService;

	private ObjectMapper om;

	private String URL = "/clients";

	@Before
	public void setup() {
		this.clientRepository = mock(ClientRepository.class);
		this.clientService = mock(ClientService.class);

		this.om = new ObjectMapper();
		this.mockMvc = MockMvcBuilders.standaloneSetup(new ClientController(clientRepository, clientService))
				.setControllerAdvice(new CustomizedResponseEntityExceptionHandler()).build();
	}

	@Test
	public void givenClient_whenGetClient_thenReturnClient() throws Exception {
		Long clientId = 1L;
		ClientModel client = new ClientModel(clientId, "Cengiz");
		
		when(clientService.getClientById(clientId)).thenReturn(client);
		MvcResult result = mockMvc.perform(get(URL + "/{id}", clientId))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
				.andReturn();
		ClientModel response = om.readValue(result.getResponse().getContentAsString(),
				new TypeReference<ClientModel>() {
				});

		assertEquals(response.getName(), client.getName());
		// TODO diğer alanlarıda ekle
	}



	@Test
	public void whenAddClient_thenShouldCreatedClient() throws Exception {
		CreateClientModel model = new CreateClientModel("Cengiz");
		when(clientRepository.save(any(Client.class))).thenAnswer(new Answer<Client>() {

			@Override
			public Client answer(InvocationOnMock invocation) throws Throwable {
				return new Client(1L, "Cengiz", true);
			}

		});

		mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(om.writeValueAsString(model)).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(status().isCreated());
	}

}
