package com.cs.mywallet.api.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cs.mywallet.api.validator.impl.CardNumberValidator;

@Constraint(validatedBy = CardNumberValidator.class)
@Documented
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidCardNumber {

    String message() default "Card number should consist of digits and length between 13-19";

    /** set true if you accept null values as valid currency */
    boolean isNullValid() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
