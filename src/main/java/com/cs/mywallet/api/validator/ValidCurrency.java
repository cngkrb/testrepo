package com.cs.mywallet.api.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cs.mywallet.api.validator.impl.CurrencyValidator;

@Constraint(validatedBy = CurrencyValidator.class)
@Documented
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidCurrency {

    String message() default "Invalid Currency. Use ISO 4217 Currency Codes";

    /** set true if you accept null values as valid currency */
    boolean isNullValid() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
