package com.cs.mywallet.api.validator.impl;

import java.util.Currency;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cs.mywallet.api.validator.ValidCurrency;

public class CurrencyValidator implements ConstraintValidator<ValidCurrency, String> {

    private boolean isNullValid;

    @Override
    public void initialize(ValidCurrency constraintAnnotation) {
        isNullValid = constraintAnnotation.isNullValid();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return isNullValid;
        }

        try {
            Currency.getInstance(value);
        } catch (IllegalArgumentException e) {
            // currency value not valid
            return false;
        }

        return true;
    }

}
