package com.cs.mywallet.api.validator.impl;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cs.mywallet.api.validator.ValidCardNumber;

public class CardNumberValidator implements ConstraintValidator<ValidCardNumber, String> {

    private Pattern pattern = Pattern.compile("[0-9]+");
    private boolean isNullValid;

    @Override
    public void initialize(ValidCardNumber constraintAnnotation) {
        isNullValid = constraintAnnotation.isNullValid();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return isNullValid;
        }
        if (pattern.matcher(value).matches()) {
            if ((value.length() >= 13) && (value.length() <= 19)) {
                // 0 leading card number check maybe ?
                return true;
            }
        }
        return false;
    }
}
