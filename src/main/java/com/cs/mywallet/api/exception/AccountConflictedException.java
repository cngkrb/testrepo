package com.cs.mywallet.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class AccountConflictedException extends RuntimeException {

    public AccountConflictedException(String exception) {
        super("Account exists. " + exception);
    }

}
