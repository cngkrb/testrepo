package com.cs.mywallet.api.exception;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class ExceptionResponse {
    @ApiModelProperty(value = "timestamp", example = "2018-05-22T05:19:45.218+0000")
    private Date   timestamp;

    @ApiModelProperty(value = "message", example = "Not Found")
    private String message;

    @ApiModelProperty(value = "details", example = "details\": \"uri=/")
    private String details;

    public ExceptionResponse(Date timestamp, String message, String details) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

}