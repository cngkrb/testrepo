package com.cs.mywallet.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ClientConflictedException extends RuntimeException {

    public ClientConflictedException(String exception) {
        super("Client exists. " + exception);
    }

}
