package com.cs.mywallet.api.controller;

import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.cs.mywallet.api.controller.model.AccountModel;
import com.cs.mywallet.api.controller.model.ClientModel;
import com.cs.mywallet.api.controller.model.CreateAccountModel;
import com.cs.mywallet.api.controller.model.CreditCardDepositModel;
import com.cs.mywallet.api.controller.model.TransactionDetailModel;
import com.cs.mywallet.api.controller.model.TransactionModel;
import com.cs.mywallet.api.dao.AccountRepository;
import com.cs.mywallet.api.dao.ClientRepository;
import com.cs.mywallet.api.dao.TransactionRepository;
import com.cs.mywallet.api.dao.model.Account;
import com.cs.mywallet.api.dao.model.Client;
import com.cs.mywallet.api.dao.model.Transaction;
import com.cs.mywallet.api.exception.AccountConflictedException;
import com.cs.mywallet.api.exception.AccountNotFoundException;
import com.cs.mywallet.api.exception.ClientNotFoundException;
import com.cs.mywallet.api.exception.ExceptionResponse;
import com.cs.mywallet.api.exception.InvalidCurrencyException;
import com.cs.mywallet.api.exception.ProvisionFailedException;
import com.cs.mywallet.api.mapper.AccountMapper;
import com.cs.mywallet.api.mapper.TransactionMapper;
import com.cs.mywallet.api.service.CreditCardProvisionService;
import com.cs.mywallet.api.service.model.CreditCardProvisionRequest;
import com.cs.mywallet.api.service.model.CreditCardProvisionResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/accounts")
@CrossOrigin
public class AccountController {
    private AccountMapper              accountMapper     = AccountMapper.INSTANCE;
    private TransactionMapper          transactionMapper = TransactionMapper.INSTANCE;

    private ClientRepository           clientRepository;
    private AccountRepository          accountRepository;
    private TransactionRepository      transactionRepository;
    private CreditCardProvisionService creditCardProvisionService;

    @Autowired
    public AccountController(ClientRepository clientRepository, AccountRepository accountRepository, TransactionRepository transactionRepository, CreditCardProvisionService creditCardProvisionService) {
        this.clientRepository = clientRepository;
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.creditCardProvisionService = creditCardProvisionService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
    @ApiOperation(value = "Returns account info for given account id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AccountModel.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class) })
    public ResponseEntity<AccountModel> getAccountById(@PathVariable("id") @ApiParam(value = "id", example = "1") Long id) {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            AccountModel model = accountMapper.toAccountModel(account.get());
            return new ResponseEntity<>(model, HttpStatus.OK);
        } else {
            throw new AccountNotFoundException("id:" + id);
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/client/{clientId}", produces = "application/json")
    @ApiOperation(value = "Returns account info for given account id and currency(optional)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AccountModel.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class) })
    public ResponseEntity<List<AccountModel>> getAccounts(@PathVariable("clientId") @ApiParam(value = "clientId", example = "1") Long clientId,
            @ApiParam(value = "currency", example = "TRY") @RequestParam(required = false, name = "currency") String currency) {
        if (currency == null) {
            // return all
            List<Account> accounts = accountRepository.findByClientIdAndActive(clientId, true);
            if (!accounts.isEmpty()) {
                List<AccountModel> model = accountMapper.toAccountModelList(accounts);
                return new ResponseEntity<>(model, HttpStatus.OK);
            } else {
                throw new AccountNotFoundException("clientId:" + clientId);
            }
        }
        try {
            Currency.getInstance(currency);
        } catch (IllegalArgumentException e) {
            // currency value not valid
            throw new InvalidCurrencyException("currency:" + currency);
        }

        Optional<Account> optionalAccount = accountRepository.findByClientIdAndCurrencyAndActive(clientId, currency, true);
        if (optionalAccount.isPresent()) {
            AccountModel model = accountMapper.toAccountModel(optionalAccount.get());
            List<AccountModel> accountList = new ArrayList<>();
            accountList.add(model);
            return new ResponseEntity<>(accountList, HttpStatus.OK);
        } else {
            throw new AccountNotFoundException("clientId:" + clientId + " currency:" + currency);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}", produces = "application/json")
    @ApiOperation(value = "deletes corresponding account entity for given account id ")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No Content", response = Void.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class) })
    public ResponseEntity<Void> deleteAccountById(@PathVariable("id") @ApiParam(value = "id", example = "1") Long id) {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            // account.
            Account accountEntity = account.get();
            accountEntity.setActive(false);
            Account savedAccountEntity = accountRepository.save(accountEntity);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new AccountNotFoundException("id:" + id);
        }

    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json", path = "")
    @ApiOperation(value = "Create new account")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = AccountModel.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class) })
    public ResponseEntity<AccountModel> createAccount(@Valid @RequestBody CreateAccountModel request, UriComponentsBuilder ucBuilder) {
        Optional<Client> client = clientRepository.findById(request.getClientId());
        if (!client.isPresent()) {
            // client not found
            throw new ClientNotFoundException("id:" + request.getClientId());
        }

        Optional<Account> optionalAccount = accountRepository.findByClientIdAndCurrencyAndActive(request.getClientId(), request.getCurrency(), true);
        if (optionalAccount.isPresent()) {
            // account already exists
            throw new AccountConflictedException("clientId:" + request.getClientId() + " currency:" + request.getCurrency());
        }

        Account entity = accountMapper.toAccountEntity(request);
        // defaults
        entity.setActive(true);
        if (entity.getBalance() == null) {
            entity.setBalance(BigDecimal.ZERO);
        }

        Account savedEntity = accountRepository.save(entity);
        URI location = ucBuilder.path("/{id}").buildAndExpand(savedEntity.getId()).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        AccountModel model = accountMapper.toAccountModel(savedEntity);
        return new ResponseEntity<>(model, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/deposit/creditcard", produces = "application/json")
    @ApiOperation(value = "make deposit with credit card")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = TransactionModel.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class) })
    public ResponseEntity<TransactionModel> depositViaCreditCard(@Valid @RequestBody CreditCardDepositModel request) {
        // this is not the "proper" way to handle transaction
        // atomicity and consistency ignored here

        Optional<Account> account = accountRepository.findById(request.getAccountId());
        if (!account.isPresent()) {
            // account not found
            throw new AccountNotFoundException("id:" + request.getAccountId());
        }
        Account accountEntity = account.get();

        CreditCardProvisionRequest provisionRequest = new CreditCardProvisionRequest();
        provisionRequest.setAmount(request.getAmount());
        provisionRequest.setCardNumber(request.getCardNumber());
        provisionRequest.setCurrency(accountEntity.getCurrency());
        CreditCardProvisionResponse creditCardProvisionResponse = creditCardProvisionService.getProvision(provisionRequest);
        if (creditCardProvisionResponse.isProvision()) {
            Transaction transaction = new Transaction();
            transaction.setAmount(request.getAmount());
            transaction.setReceiver(String.valueOf(request.getAccountId()));
            transaction.setSender(request.getCardNumber());
            transaction.setTransactionType("CC-Deposit");
            transaction.setProvisionId(creditCardProvisionResponse.getProvisionId());

            Transaction savedTransaction = transactionRepository.save(transaction);

            BigDecimal currenctBalance = accountEntity.getBalance();
            accountEntity.setBalance(currenctBalance.add(savedTransaction.getAmount()));
            Account updatedEntity = accountRepository.save(accountEntity);

            TransactionModel model = transactionMapper.toTransactionModel(savedTransaction);
            return new ResponseEntity<>(model, HttpStatus.OK);
        } else {
            // no provision
            throw new ProvisionFailedException(provisionRequest.toString());
        }
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json", path = "/{id}/transactions")
    @ApiOperation(value = "Returns transaction history info for account id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = TransactionDetailModel.class),
            @ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class) })
    public ResponseEntity<TransactionDetailModel> getTransactions(@PathVariable("id") @ApiParam(value = "id", example = "1") Long id) {
        Optional<Account> account = accountRepository.findById(id);
        if (!account.isPresent()) {
            // account not found
            throw new AccountNotFoundException("id:" + id);
        }

        List<Transaction> send = transactionRepository.findBySender(String.valueOf(id));
        List<Transaction> received = transactionRepository.findByReceiver(String.valueOf(id));

        TransactionDetailModel model = new TransactionDetailModel();
        model.setSend(transactionMapper.toTransactionModelList(send));
        model.setReceived(transactionMapper.toTransactionModelList(received));
        return new ResponseEntity<>(model, HttpStatus.OK);
    }
}
