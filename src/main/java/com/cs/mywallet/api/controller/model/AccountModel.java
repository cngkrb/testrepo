package com.cs.mywallet.api.controller.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class AccountModel {
    @ApiModelProperty(value = "id", example = "1987654")
    private Long       id;

    @ApiModelProperty(value = "clientId", example = "2546")
    private Long       clientId;

    @ApiModelProperty(value = "balance", example = "200.20")
    private BigDecimal balance;

    @ApiModelProperty(value = "name", example = "Döviz-USD")
    private String     name;

    @ApiModelProperty(value = "currency", example = "USD")
    private String     currency;

    @ApiModelProperty(value = "active", example = "true")
    private boolean    active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
