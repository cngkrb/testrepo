package com.cs.mywallet.api.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.cs.mywallet.api.controller.model.ClientModel;
import com.cs.mywallet.api.controller.model.CreateClientModel;
import com.cs.mywallet.api.dao.ClientRepository;
import com.cs.mywallet.api.dao.model.Client;
import com.cs.mywallet.api.exception.ExceptionResponse;
import com.cs.mywallet.api.mapper.ClientMapper;
import com.cs.mywallet.api.service.ClientService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/clients")
@CrossOrigin
public class ClientController {

	private ClientMapper mapper = ClientMapper.INSTANCE;

	private ClientRepository clientRepository;

	private ClientService clientService;

	@Autowired
	public ClientController(ClientRepository clientRepository, ClientService clientService) {
		this.clientRepository = clientRepository;
		this.clientService = clientService;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = "application/json")
	@ApiOperation(value = "Returns detailed client model for given client id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ClientModel.class),
			@ApiResponse(code = 404, message = "Not Found", response = ExceptionResponse.class) })
	public ResponseEntity<ClientModel> getClient(
			@PathVariable("id") @ApiParam(value = "client id", example = "1") Long id) {
		ClientModel model = clientService.getClientById(id);
		return new ResponseEntity<>(model, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, path = "", produces = "application/json")
	@ApiOperation(value = "create new client")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Created", response = ClientModel.class) })
	public ResponseEntity<ClientModel> addClient(@Valid @RequestBody CreateClientModel request,
			UriComponentsBuilder ucBuilder) {
		Client entity = mapper.toClientEntity(request);
		entity.setActive(true);

		Client savedEntity = clientRepository.save(entity);
		ClientModel model = mapper.toClientModel(savedEntity);

		URI location = ucBuilder.path("/{id}").buildAndExpand(savedEntity.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(location);

		return new ResponseEntity<>(model, headers, HttpStatus.CREATED);
	}

}
