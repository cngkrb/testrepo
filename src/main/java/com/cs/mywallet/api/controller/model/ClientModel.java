package com.cs.mywallet.api.controller.model;

import io.swagger.annotations.ApiModelProperty;

public class ClientModel {

	@ApiModelProperty(value = "id", example = "23")
	private Long id;

	@ApiModelProperty(value = "name", example = "Cengiz Karaboz")
	private String name;

	public ClientModel() {
		// Auto-generated constructor stub
	}

	public ClientModel(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
