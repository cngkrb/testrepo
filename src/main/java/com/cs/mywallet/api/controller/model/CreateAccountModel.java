package com.cs.mywallet.api.controller.model;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.cs.mywallet.api.validator.ValidCurrency;

import io.swagger.annotations.ApiModelProperty;

public class CreateAccountModel {

    @NotNull
    @ApiModelProperty(value = "clientId", required = true, example = "1")
    private Long       clientId;

    @ApiModelProperty(value = "balance", required = false, example = "6.02")
    private BigDecimal balance;

    @NotNull
    @ApiModelProperty(value = "name", required = true, example = "Birikim")
    private String     name;

    @ValidCurrency
    @ApiModelProperty(value = "currency", required = true, example = "TRY")
    private String     currency;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
