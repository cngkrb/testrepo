package com.cs.mywallet.api.controller.model;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.cs.mywallet.api.validator.ValidCardNumber;

import io.swagger.annotations.ApiModelProperty;

public class CreditCardDepositModel {
    @ValidCardNumber
    @ApiModelProperty(value = "card number where deposit is made from", required = true, example = "4949000012124747")
    private String     cardNumber;

    @NotNull
    @ApiModelProperty(value = "target account number", required = true, example = "2")
    private Long       accountId;

    @NotNull
    @ApiModelProperty(value = "amount of deposit", required = true, example = "100.01")
    private BigDecimal amount;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
