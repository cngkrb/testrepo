package com.cs.mywallet.api.controller.model;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class CreateClientModel {

    @NotNull
    @ApiModelProperty(value = "name", required = true, example = "Cengiz Karaboz")
    private String name;

    public CreateClientModel() {
        super();
    }

    public CreateClientModel(@NotNull String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
