package com.cs.mywallet.api.controller.model;

import java.util.List;

public class TransactionDetailModel {

    private List<TransactionModel> send;
    private List<TransactionModel> received;

    public List<TransactionModel> getSend() {
        return send;
    }

    public void setSend(List<TransactionModel> send) {
        this.send = send;
    }

    public List<TransactionModel> getReceived() {
        return received;
    }

    public void setReceived(List<TransactionModel> received) {
        this.received = received;
    }

}
