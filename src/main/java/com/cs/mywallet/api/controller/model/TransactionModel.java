package com.cs.mywallet.api.controller.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class TransactionModel {
    @ApiModelProperty(value = "id", example = "98463521")
    private Long       id;
    
    @ApiModelProperty(value = "sender", example = "4949000012124747")
    private String     sender;
    
    @ApiModelProperty(value = "receiver", example = "654632")
    private String     receiver;
    
    @ApiModelProperty(value = "transactionType", example = "CC-Deposit")
    private String     transactionType;
    
    @ApiModelProperty(value = "amount", example = "2300")
    private BigDecimal amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
