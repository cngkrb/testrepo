package com.cs.mywallet.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cs.mywallet.api.controller.model.ClientModel;
import com.cs.mywallet.api.controller.model.CreateClientModel;
import com.cs.mywallet.api.dao.model.Client;

@Mapper
public abstract class ClientMapper {
    public static final ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

    public abstract ClientModel toClientModel(Client entity);

    public abstract Client toClientEntity(CreateClientModel entity);
}
