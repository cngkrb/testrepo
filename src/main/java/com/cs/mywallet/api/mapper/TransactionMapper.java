package com.cs.mywallet.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cs.mywallet.api.controller.model.TransactionModel;
import com.cs.mywallet.api.dao.model.Transaction;

@Mapper
public abstract class TransactionMapper {
    public static final TransactionMapper INSTANCE = Mappers.getMapper(TransactionMapper.class);

    public abstract TransactionModel toTransactionModel(Transaction entity);

    public abstract List<TransactionModel> toTransactionModelList(List<Transaction> entity);
}
