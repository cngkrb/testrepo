package com.cs.mywallet.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cs.mywallet.api.controller.model.AccountModel;
import com.cs.mywallet.api.controller.model.CreateAccountModel;
import com.cs.mywallet.api.dao.model.Account;

@Mapper
public abstract class AccountMapper {
    public static final AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    public abstract AccountModel toAccountModel(Account entity);

    public abstract List<AccountModel> toAccountModelList(List<Account> entityList);

    public abstract Account toAccountEntity(CreateAccountModel entity);
}
