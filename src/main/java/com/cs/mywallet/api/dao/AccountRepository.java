package com.cs.mywallet.api.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cs.mywallet.api.dao.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long>, JpaSpecificationExecutor<Account> {

    public Optional<Account> findByClientIdAndCurrency(Long clientId, String currency);

    public Optional<Account> findByClientIdAndCurrencyAndActive(Long clientId, String currency, boolean active);

    public List<Account> findByClientId(Long clientId);

    public List<Account> findByClientIdAndActive(Long clientId, boolean active);

}
