package com.cs.mywallet.api.dao.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "ACCOUNT", uniqueConstraints = @UniqueConstraint(columnNames = { "CLIENT_ID", "CURRENCY", "ACTIVE" }))
public class Account {

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "SEQ_ACCOUNT", sequenceName = "SEQ_ACCOUNT", initialValue = 100, allocationSize = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ACCOUNT")
    private Long             id;

    @Column(name = "CLIENT_ID", nullable = false)
    private Long             clientId;

    @Column(name = "BALANCE", nullable = false)
    private BigDecimal       balance;

    @Column(name = "NAME", nullable = false)
    private String           name;

    // ISO 4217 Currency Codes
    @Column(name = "CURRENCY", nullable = false)
    private String           currency;

    @Column(name = "ACTIVE", nullable = false)
    private Boolean          active;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENT_ID", insertable = false, updatable = false)
    private Client           client;

    @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Transaction> receivedtransactions;

    public Account() {
        super();
    }

    public Account(Long id, Long clientId, BigDecimal balance, String name, String currency, Boolean active) {
        super();
        this.id = id;
        this.clientId = clientId;
        this.balance = balance;
        this.name = name;
        this.currency = currency;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Transaction> getReceivedtransactions() {
        return receivedtransactions;
    }

    public void setReceivedtransactions(Set<Transaction> receivedtransactions) {
        this.receivedtransactions = receivedtransactions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
