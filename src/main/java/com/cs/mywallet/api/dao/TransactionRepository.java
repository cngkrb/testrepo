package com.cs.mywallet.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cs.mywallet.api.dao.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long>, JpaSpecificationExecutor<Transaction> {

    public List<Transaction> findBySender(String sender);

    public List<Transaction> findByReceiver(String receiver);
}
