package com.cs.mywallet.api.dao.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTION")
public class Transaction {

    @Id
    @Column(name = "ID", nullable = false)
    @SequenceGenerator(name = "SEQ_TRANSACTION", sequenceName = "SEQ_TRANSACTION", initialValue = 1, allocationSize = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TRANSACTION")
    private Long       id;

    @Column(name = "SENDER")
    private String     sender;

    @Column(name = "RECEIVER")
    private String     receiver;

    @Column(name = "TRANSACTION_TYPE", nullable = false)
    private String     transactionType;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;

    @Column(name = "PROVISION_ID", nullable = false)
    private String     provisionId;

    public String getProvisionId() {
        return provisionId;
    }

    public void setProvisionId(String provisionId) {
        this.provisionId = provisionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
