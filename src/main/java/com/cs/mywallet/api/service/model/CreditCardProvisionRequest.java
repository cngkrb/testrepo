package com.cs.mywallet.api.service.model;

import java.math.BigDecimal;

public class CreditCardProvisionRequest {

    private String     cardNumber;
    private String     currency;
    private BigDecimal amount;

    public CreditCardProvisionRequest() {
        super();
    }

    public CreditCardProvisionRequest(String cardNumber, String currency, BigDecimal amount) {
        super();
        this.cardNumber = cardNumber;
        this.currency = currency;
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public String toString() {
        return "CreditCardProvisionRequest [cardNumber=" + cardNumber + ", currency=" + currency + ", amount=" + amount + "]";
    }

}
