package com.cs.mywallet.api.service;

import com.cs.mywallet.api.controller.model.ClientModel;
import com.cs.mywallet.api.exception.ClientNotFoundException;

public interface ClientService {

	public ClientModel getClientById(long id) throws ClientNotFoundException;

}
