package com.cs.mywallet.api.service.model;

public class CreditCardProvisionResponse {

    private boolean provision;
    private String  provisionId;

    public CreditCardProvisionResponse() {
        super();
    }

    public CreditCardProvisionResponse(boolean provision, String provisionId) {
        super();
        this.provision = provision;
        this.provisionId = provisionId;
    }

    public boolean isProvision() {
        return provision;
    }

    public void setProvision(boolean provision) {
        this.provision = provision;
    }

    public String getProvisionId() {
        return provisionId;
    }

    public void setProvisionId(String provisionId) {
        this.provisionId = provisionId;
    }

}
