package com.cs.mywallet.api.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.cs.mywallet.api.controller.model.ClientModel;
import com.cs.mywallet.api.dao.ClientRepository;
import com.cs.mywallet.api.dao.model.Client;
import com.cs.mywallet.api.exception.ClientNotFoundException;
import com.cs.mywallet.api.mapper.ClientMapper;

@Service
public class ClientServiceImpl implements ClientService {
	private ClientMapper mapper = ClientMapper.INSTANCE;

	private ClientRepository clientRepository;

	public ClientServiceImpl(ClientRepository clientRepository) {
		super();
		this.clientRepository = clientRepository;
	}

	@Override
	public ClientModel getClientById(long id) throws ClientNotFoundException {
		Optional<Client> client = clientRepository.findById(id);
		if (client.isPresent()) {
			return mapper.toClientModel(client.get());
		} else {
			throw new ClientNotFoundException("id:" + id);
		}

	}
}
