package com.cs.mywallet.api.service;

import java.util.UUID;

import org.springframework.stereotype.Service;

import com.cs.mywallet.api.service.model.CreditCardProvisionRequest;
import com.cs.mywallet.api.service.model.CreditCardProvisionResponse;

@Service
public class CreditCardProvisionService {

    public CreditCardProvisionResponse getProvision(CreditCardProvisionRequest request) {
        // return class should be more detailed
        // for the sake of mocking this service, simply returning true for provision
        CreditCardProvisionResponse response = new CreditCardProvisionResponse();
        response.setProvision(true);
        response.setProvisionId(UUID.randomUUID().toString());
        return response;
    }
}
