--SEQ 1-100 reserved
INSERT INTO client (id, name, active) VALUES (1, 'cengiz karaboz',1);
INSERT INTO client (id, name, active) VALUES (2, 'account 101', 1 );
INSERT INTO client (id, name, active) VALUES (3, 'account disabled', 0 );

INSERT INTO account (id, CLIENT_ID,BALANCE, NAME,CURRENCY,ACTIVE) VALUES (1, 1, 333,'maas','TRY',1 );
INSERT INTO account (id, CLIENT_ID,BALANCE, NAME,CURRENCY,ACTIVE) VALUES (2, 1, 444,'maas2','GBP',1 );
INSERT INTO account (id, CLIENT_ID,BALANCE, NAME,CURRENCY,ACTIVE) VALUES (3, 1, 555,'maas3','USD',1 );
INSERT INTO account (id, CLIENT_ID,BALANCE, NAME,CURRENCY,ACTIVE) VALUES (4, 1, 222,'maas4','EUR',0 );
